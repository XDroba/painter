/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QLabel *label;
    QSpinBox *Red;
    QSpinBox *Green;
    QSpinBox *Blue;
    QLabel *label_2;
    QSpinBox *pocet;
    QLabel *label_3;
    QSpinBox *polomer;
    QComboBox *typ;
    QPushButton *kresli;
    QPushButton *zmaz;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEffects;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(613, 494);
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 591, 160));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        horizontalLayout->addWidget(scrollArea);


        verticalLayout->addLayout(horizontalLayout);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        Red = new QSpinBox(centralWidget);
        Red->setObjectName(QStringLiteral("Red"));
        Red->setMaximum(255);

        verticalLayout->addWidget(Red);

        Green = new QSpinBox(centralWidget);
        Green->setObjectName(QStringLiteral("Green"));
        Green->setMaximum(255);

        verticalLayout->addWidget(Green);

        Blue = new QSpinBox(centralWidget);
        Blue->setObjectName(QStringLiteral("Blue"));
        Blue->setMaximum(255);

        verticalLayout->addWidget(Blue);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        pocet = new QSpinBox(centralWidget);
        pocet->setObjectName(QStringLiteral("pocet"));
        pocet->setMinimum(3);
        pocet->setMaximum(200);

        verticalLayout->addWidget(pocet);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        polomer = new QSpinBox(centralWidget);
        polomer->setObjectName(QStringLiteral("polomer"));
        polomer->setMinimum(1);
        polomer->setMaximum(300);

        verticalLayout->addWidget(polomer);

        typ = new QComboBox(centralWidget);
        typ->setObjectName(QStringLiteral("typ"));

        verticalLayout->addWidget(typ);

        kresli = new QPushButton(centralWidget);
        kresli->setObjectName(QStringLiteral("kresli"));

        verticalLayout->addWidget(kresli);

        zmaz = new QPushButton(centralWidget);
        zmaz->setObjectName(QStringLiteral("zmaz"));

        verticalLayout->addWidget(zmaz);

        MyPainterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 613, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEffects = new QMenu(menuBar);
        menuEffects->setObjectName(QStringLiteral("menuEffects"));
        MyPainterClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEffects->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuEffects->addAction(actionClear);
        menuEffects->addAction(actionNegative);
        menuEffects->addAction(actionMedium_Negative);
        menuEffects->addAction(actionSlow_Negative);
        menuEffects->addAction(actionSalt_Pepper);
        menuEffects->addAction(actionMedian_filter);
        menuEffects->addAction(actionRotate_left);
        menuEffects->addAction(actionRotate_right);

        retranslateUi(MyPainterClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), MyPainterClass, SLOT(ActionOpen()));
        QObject::connect(actionSave, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSave()));
        QObject::connect(actionClear, SIGNAL(triggered()), MyPainterClass, SLOT(EffectClear()));
        QObject::connect(actionNew, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNew()));
        QObject::connect(actionNegative, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNegative()));
        QObject::connect(actionSlow_Negative, SIGNAL(triggered()), MyPainterClass, SLOT(ActionBlackWhite()));
        QObject::connect(actionMedium_Negative, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSepiaTone()));
        QObject::connect(actionMedian_filter, SIGNAL(triggered()), MyPainterClass, SLOT(ActionMedian()));
        QObject::connect(actionSalt_Pepper, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSaltPepper()));
        QObject::connect(actionRotate_left, SIGNAL(triggered()), MyPainterClass, SLOT(ActionLeft()));
        QObject::connect(actionRotate_right, SIGNAL(triggered()), MyPainterClass, SLOT(ActionRight()));
        QObject::connect(kresli, SIGNAL(clicked()), MyPainterClass, SLOT(kresli()));
        QObject::connect(zmaz, SIGNAL(clicked()), MyPainterClass, SLOT(zmaz()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", 0));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", 0));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", 0));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", 0));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", 0));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", 0));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", 0));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", 0));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", 0));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", 0));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", 0));
        label->setText(QApplication::translate("MyPainterClass", "RGB", 0));
        label_2->setText(QApplication::translate("MyPainterClass", "Pocet", 0));
        label_3->setText(QApplication::translate("MyPainterClass", "Polomer", 0));
        typ->clear();
        typ->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "DDA", 0)
         << QApplication::translate("MyPainterClass", "Bresenhamov", 0)
        );
        kresli->setText(QApplication::translate("MyPainterClass", "kresli", 0));
        zmaz->setText(QApplication::translate("MyPainterClass", "zmaz", 0));
        menuFile->setTitle(QApplication::translate("MyPainterClass", "File", 0));
        menuEffects->setTitle(QApplication::translate("MyPainterClass", "Effects", 0));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
