#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <QElapsedTimer>

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();
	int pocet;
	int polomer;
	int red;
	int green;
	int blue;
	int typ;

public slots:
	void kresli();
	void zmaz();
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void ActionNegative();
	void ActionBlackWhite();
	void ActionSepiaTone();
	void ActionMedian();
	void ActionSaltPepper();
	void ActionLeft();
	void ActionRight();

private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;
};

#endif // MYPAINTER_H
