#include "paintwidget.h"
#include <math.h>


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::fastNegative()
{
	int x = image.height();
	int y = image.width();
	unsigned __int64 asd = 0x00ffffff00ffffff;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		QRgb* scanline=(QRgb*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			scanline[j] = 16777215 - scanline[j];
		}
		/*unsigned __int64* scanline=(unsigned __int64*)image.scanLine(i);
		for (int j = 0; j < y/2; j++)
		{
			scanline[j] ^= asd;
		}*/
	}
	update();
}

void PaintWidget::blackWhite()
{

	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			uchar color = (scanline[j * 4 + 0] * 0.11) + (scanline[j * 4 + 1] * 0.59) + (scanline[j * 4 + 2] * 0.3);
			scanline[j * 4 + 0] = color;
			scanline[j * 4 + 1] = color;
			scanline[j * 4 + 2] = color;
		}
	}
	/*for (int i = 0; i < image.width(); i++)
	{
		for (int j = 0; j < image.height(); j++)
		{
			QColor farba = image.pixelColor(i, j);
			farba.setRed(255 - farba.red());
			farba.setBlue(255 - farba.blue());
			farba.setGreen(255 - farba.green());
			image.setPixelColor(i, j, farba);
		}
	}*/
	update();
}

void PaintWidget::sepiaTone()
{
	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			int colorR = scanline[j * 4 + 2] * 0.393 + scanline[j * 4 + 1] * 0.769 + scanline[j * 4 + 0] * 0.189;
			int colorG = scanline[j * 4 + 2] * 0.349 + scanline[j * 4 + 1] * 0.686 + scanline[j * 4 + 0] * 0.168;
			int colorB = scanline[j * 4 + 2] * 0.272 + scanline[j * 4 + 1] * 0.534 + scanline[j * 4 + 0] * 0.131;
			scanline[j * 4 + 2] = colorR > 255 ? 255 : colorR;
			scanline[j * 4 + 1] = colorG > 255 ? 255 : colorG;
			scanline[j * 4 + 0] = colorB > 255 ? 255 : colorB;
		}
	}
	/*
	outputRed = (inputRed * .393) + (inputGreen *.769) + (inputBlue * .189)
outputGreen = (inputRed * .349) + (inputGreen *.686) + (inputBlue * .168)
outputBlue = (inputRed * .272) + (inputGreen *.534) + (inputBlue * .131)*/
	/*for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			QRgb farba = image.pixel(j, i);
			image.setPixel(j, i, 16777215 - farba);
		}
	}*/
	update();
}

void PaintWidget::medianFilter()
{
	
	int x = image.height();
	int y = image.width();
	uchar *data = image.bits();
	int riadok = image.bytesPerLine();
#pragma omp parallel
	{
		int reds[9];
		int greens[9];
		int blues[9];
#pragma omp for
		for (int i = 1; i < x - 1; i++)
		{
			for (int j = 1; j < y - 1; j++)
			{
				int counter = 0;
				for (int k = -1; k < 2; k++)
					for (int l = -1; l < 2; l++)
					{
						blues[counter] = data[(i + k)*riadok + (j + l) * 4 + 0];
						greens[counter] = data[(i + k)*riadok + (j + l) * 4 + 1];
						reds[counter] = data[(i + k)*riadok + (j + l) * 4 + 2];
						counter++;
					}
				data[i*riadok + j * 4 + 0] = blues[selectKth(blues, 0, 9, 4)];
				data[i*riadok + j * 4 + 1] = greens[selectKth(greens, 0, 9, 4)];
				data[i*riadok + j * 4 + 2] = reds[selectKth(reds, 0, 9, 4)];
			}
		}
	}
}

void PaintWidget::saltPepper()
{
	int count = image.width()*image.height()*0.1;
	QRgb *data = (QRgb *)image.bits();
	int riadok = image.bytesPerLine()/4;
#pragma omp parallel
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> xsur(0, image.width() - 1);
		std::uniform_int_distribution<> ysur(0, image.height() - 1);
#pragma omp for
		for (int i = 0; i < count; i++)
		{
			data[ysur(gen)*riadok +xsur(gen)] = i % 2 == 0 ? 0x00000000 : 0x00ffffff;
		}
	}
}

void PaintWidget::RotateLeft()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine()/4;
	int riadok2 = druhy.bytesPerLine()/4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[i + (y-j-1)*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::RotateRight()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine() / 4;
	int riadok2 = druhy.bytesPerLine() / 4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[x - i - 1 + j*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::kresli(int pocet, int polomer, int red, int green, int blue, int typ)
{
	QPainter painter(&image);
	QMessageBox mbox;
	int ys = image.height() / 2;
	int xs = image.width() / 2;
	
	painter.setPen(QPen(QColor(red, green, blue), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(QColor(red, green, blue)));
	
	for (int i = 0; i < body.size() - 1; i++)
	{
		this->dda(&painter, body.at(i).x(), body.at(i).y(), body.at(i + 1).x(), body.at(i + 1).y());
	}
	
	this->dda(&painter, body.at(0).x(), body.at(0).y(), body.at(body.size() - 1).x(), body.at(body.size() - 1).y());

	int j = 1;
	double dy;
	double dx;

	for (int i = 0; i < body.size(); i++)
	{
		if (i == body.size() - 1)
		{
			j = 0;
		}

		dy = (double)body.at(j).y() - (double)body.at(i).y();
		dx = (double)body.at(j).x() - (double)body.at(i).x();
		
		if (dy != 0)
		{
			if (dy < 0)
			{
				help.append((double)body.at(i).x());
				help.append((double)body.at(i).y());
				help.append((double)body.at(j).y());
			}
			else
			{
				help.append((double)body.at(j).x());
				help.append((double)body.at(j).y());
				help.append((double)body.at(i).y());
			}
			help.append(dy / dx);
		}
		th.append(help);
		
		help.clear();
		
		j++;
	}

	for (int i = 0; i < th.size() - 1; i++)
	{
		QVector <double> help2;

		for (int j = 0; j < th.size() - 1; j++)
		{
			if (th[j + 1][1] > th[j][1])
			{
				help2 = th[j + 1];
				th[j + 1] = th[j];
				th[j] = help2;
			}
		}		
	}

	for (int i = 0; i < th.size() - 1; i++)
	{
		QVector <double> help2;

		for (int j = 0; j < th.size() - 1; j++)
		{
			if (th[j + 1][1] == th[j][1] && th[j + 1][0] < th[j][0])
			{
				help2 = th[j + 1];
				th[j + 1] = th[j];
				th[j] = help2;
			}
		}
	}
	for (int i = 0; i < th.size() - 1; i++)
	{
		QVector <double> help2;

		for (int j = 0; j < th.size() - 1; j++)
		{
			if (th[j + 1][1] == th[j][1] && th[j + 1][0] == th[j][0] && th[j + 1][1] > th[j][1])
			{
				help2 = th[j + 1];
				th[j + 1] = th[j];
				th[j] = help2;
			}
		}
	}
	for (int i = 0; i < th.size(); i++)
	{
		printf("---TH-----------\n%d\t%d\t%d\t%.2lf\n", (int)th.at(i).at(0), (int)th.at(i).at(1), (int)th.at(i).at(2), th.at(i).at(3));
	}

	//for (int i = 0; i < th.size(); i++)
	//{
	//	th[i][1] = th.at(i).at(1) - 1.0;
	//	th[i][0] = round(th.at(i).at(3)) + th.at(i).at(0);
	//	printf("---TH-----------\n%d\t%d\t%d\t%.2lf\n", (int)th.at(i).at(0), (int)th.at(i).at(1), (int)th.at(i).at(2), th.at(i).at(3));
	//}
	
	int cury;
	int curx;
	j = 0;

	cury = th.at(1).at(1);
	
	while (cury != th.at(th.size() - 1).at(2))
	{
		
		//printf("test1\n");
		for (int i = 0; i < th.size(); i++)
		{
			//printf("test2\n");
			if (cury <= th.at(i).at(1) && cury > th.at(i).at(2))
			{
				//printf("test3\n");
				tah.append(th.at(i));
				//
				//j++;
			}
		}
		for (int i = 0; i < tah.size(); i++)
		{
			//printf("---TAH-----------\n%d\t%d\t%d\n", (int)tah.at(i).at(0), (int)tah.at(i).at(1), (int)tah.at(i).at(2));
			if (i % 2 == 0 || i == 0)
			{
				curx = (1 / tah[i].at(3)) * (cury - tah[i].at(1)) + tah[i].at(0);
				if (abs(tah[i + 1].at(3)) < 1)
				{
					while (curx < tah[i + 1].at(3) * (cury - tah[i + 1].at(1)) + tah[i + 1].at(0))
					{
						//printf("test5\n");
						painter.drawPoint(curx, cury);
						curx++;
					}
				}
				else
				{
					while (curx < (1 / tah[i + 1].at(3)) * (cury - tah[i + 1].at(1)) + tah[i + 1].at(0))
					{
						//printf("test5\n");
						painter.drawPoint(curx, cury);
						curx++;
					} 
				}
							
			}
		}
		cury--;
		tah.clear();
	}
/*
	for (int i = 0; i < body.size() - 1;i++)
	{

	}
	

	
	for (int i = 0; i < pocet; i++)
	{
		double x = xs + polomer * cos((i * 2 * M_PI) / pocet);
		double y = ys + polomer * sin((i * 2 * M_PI) / pocet);
		double xx = xs + polomer * cos(((i + 1) * 2 * M_PI) / pocet);
		double yy = ys + polomer * sin(((i + 1) * 2 * M_PI) / pocet);






		//painter.drawPoint((int)x, (int)y);

		if (typ == 0)
		{
			this->dda(&painter, x, y, xx, yy);
			//mbox.setText("DDA");
			//mbox.exec();
		}
		else
		{
			this->bresenham(&painter, x, y, xx, yy);
			//mbox.setText("Bresenhamov");
			//mbox.exec();
		}

	}
*/	
}

void PaintWidget::dda(QPainter *painter, int x, int y, int xx, int yy)
{
	int helpx, helpy;

	double m;

	if (xx < x)
	{
		helpx = xx;
		xx = x;
		x = helpx;
		helpy = yy;
		yy = y;
		y = helpy;
	}

	double bodx = (double)x;
	double body = (double)y;

	int dx = abs(xx - x);
	int dy = abs(yy - y);

	if (dx >= dy)
	{
		m = (double)dy / (double)dx;
	}
	else
	{
		m = (double)dx / (double)dy;
	}

	do
	{
		if (y > yy)
		{
			if (dy < dx)
			{
				painter->drawPoint((int)round(bodx), (int)round(body));
				bodx = bodx + 1;
				body = body - m;
			}
			else
			{
				painter->drawPoint((int)round(bodx), (int)round(body));
				bodx = bodx + m;
				body = body - 1;
			}
		}
		else
		{
			if (dy < dx)
			{
				painter->drawPoint((int)round(bodx), (int)round(body));
				bodx = bodx + 1;
				body = body + m;
			}
			else
			{
				painter->drawPoint((int)round(bodx), (int)round(body));
				bodx = bodx + m;
				body = body + 1;
			}
		}
	} while ((int)round(bodx) != xx || (int)round(body) != yy);
	painter->drawPoint(xx, yy);
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{

	if (event->button() == Qt::LeftButton)
	{		
		lastPoint = event->pos();
		body.append(lastPoint);
		painting = true;
	}
}

void PaintWidget::bresenham(QPainter *painter, int x, int y, int xx, int yy)
{

	int i = 0;
	int helpx, helpy;

	double m;

	if (xx < x)
	{
		helpx = xx;
		xx = x;
		x = helpx;
		helpy = yy;
		yy = y;
		y = helpy;
	}

	int bodx = x;
	int body = y;

	int dx = abs(xx - x);
	int dy = abs(yy - y);

	int r2 = 2 * dy - 2 * dx;
	int r1 = 2 * dy;
	int k2 = 2 * dx - 2 * dy;
	int k1 = 2 * dx;
	int p1 = r1 - dx;
	int q1 = k1 - dy;

	painter->drawPoint(bodx,body);

	do
	{
		if (y < yy)
		{
			if (dy < dx)
			{

				bodx = bodx + 1;
				if (p1 > 0)
				{
					body = body + 1;
					p1 = p1 + r2;
				}
				else
				{
					p1 = p1 + r1;
				}
				painter->drawPoint(bodx, body);
			}
			else
			{
				body = body + 1;
				if (q1 > 0)
				{
					bodx = bodx + 1;
					q1 = q1 + k2;
				}
				else
				{
					q1 = q1 + k1;
				}
				painter->drawPoint(bodx, body);
			}
		}
		else
		{
			if (dy < dx)
			{

				bodx = bodx + 1;
				if (p1 > 0)
				{
					body = body - 1;
					p1 = p1 + r2;
				}
				else
				{
					p1 = p1 + r1;
				}
				painter->drawPoint(bodx, body);
			}
			else
			{
				body = body - 1;
				if (q1 > 0)
				{
					bodx = bodx + 1;
					q1 = q1 + k2;
				}
				else
				{
					q1 = q1 + k1;
				}
				painter->drawPoint(bodx, body);
			}
		}
	} while (bodx < xx || body < yy || body > yy);
	painter->drawPoint(xx, yy); 
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}



void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);

	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	//painter.drawLine(lastPoint, endPoint);
	//painter.drawPoint(lastPoint);
	//painter.drawPoint(endPoint);
	
	

	modified = true;

	
	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));

	//this->bresenham(&painter, lastPoint.x(), lastPoint.y(), endPoint.x(), endPoint.y());

	lastPoint = endPoint;

	
	/*
	body.append(newPoint);

	this->bresenham(&painter, body.at(i).x(), body.at(i).y(), body.at(i + 1).x(), body.at(i + 1).y());
	*/	
	
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

